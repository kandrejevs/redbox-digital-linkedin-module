<?php
/**
 * @category RedboxDigital
 * @package  RedboxDigital_LinkedIn
 * @author   Krišs Andrejevs
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Add new attribute for customer
$setup = Mage::getModel('customer/entity_setup', 'core_setup');
$setup->addAttribute('customer', 'linkedin_profile', array(
    'type'             => 'varchar',
    'input'            => 'text',
    'label'            => 'LinkedIn profile',
    'global'           => 1,
    'visible'          => 1,
    'required'         => 0,
    'user_defined'     => 1,
    'default'          => '0',
    'visible_on_front' => 1,
    'source'           => 'profile/entity_linkedin_profile',
));

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'linkedin_profile')
    ->setData('used_in_forms', array('adminhtml_customer', 'customer_account_create', 'customer_account_edit', 'checkout_register'))
    ->save();

$installer->endSetup();