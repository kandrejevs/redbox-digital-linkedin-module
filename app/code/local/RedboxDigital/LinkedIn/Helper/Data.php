<?php

/**
 * @category RedboxDigital
 * @package  RedboxDigital_LinkedIn
 * @author   Krišs Andrejevs
 */
class RedboxDigital_LinkedIn_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Method for getting is required option for LinkedIn profile text field
     *
     * @return mixed binary true false from database
     */
    public function getIsRequired()
    {
        return Mage::getStoreConfig('linked_in_options/values/is_required');
    }
}